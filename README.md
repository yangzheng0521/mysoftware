#### 教师们常用的一些软件

- `ZoomIt` 屏幕缩放
- `Everything` 文件检索
- `LICEcap` 录制GIF
- `Q-dir` 多窗口文件操作
- `SpaceSniffer` 硬盘图形化分析
- `winDecrypt` PDF密码清除器
- `snipaste` 屏幕截图  [Snipaste-1.16.2-x64](https://pan.baidu.com/s/1uK2Aao17Bhj2y_dqgd7E2Q)  || [Snipaste_1.15.1_PortableSoft](https://pan.baidu.com/s/1pSMwZHlsYcG0G94DO1BLWQ)